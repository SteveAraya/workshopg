<?php

namespace App\Http\Controllers;
use App\Product;
use App\Category;
use DB; 
use Illuminate\Http\Request;

class ProductsController extends Controller
{

    public function index()
    {
        $products = Product::all();
        return response()->json($products,200);
    }

    public function store(Request $request)
    {
        $product = new Product();
        $product->id_category = $request->id_category;
        $product->sku = $request->sku;
        $product->name = $request->name;
        $product->price = $request->price;
        $product->stock = $request->stock;
        $product->description = $request->description;

        $product->save();
    }

    public function update(Request $request, $id)
    {
        $product = Product::find($id);

        $id_category = $request->input('id_category');
        $sku = $request->input('sku');
        $name = $request->input('name');
        $price = $request->input('price');
        $stock = $request->input('stock');
        $description = $request->input('description');

        $product->id_category = $id_category;
        $product->sku = $sku;
        $product->name = $name;
        $product->price = $price;
        $product->stock = $stock;
        $product->description = $description;
        $product->save();
        return response()->json($product, 202);

    }


    public function destroy($id)
    {
        $product = Product::find($id);

        if($product){
            $product->delete();
            return response()->json($product, 204);  
        } else{
            return response()->json($product,404);
        }
    }
}