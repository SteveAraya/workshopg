<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Http\Controllers\Controller;
use DB;

class CategoriesController extends Controller
{

    public function index()
    {
        $categories = Category::all();
        return response()->json($categories,200);
    }

    public function store(Request $request)
    {
        $category = new Category();
        $category->name = $request->name;
        $category->description = $request->description;
        $category->save();
    }

    public function update(Request $request, $id)
    {
        $category = Category::find($id);

        $name = $request->input('name');
        $description = $request->input('description');

        $category->name = $name;
        $category->description = $description;

        $category->save();
        return response()->json($category, 202);
    }

    public function destroy($id)
    {
        $category = Category::find($id);

        if($category){
            $category->delete();
            return response()->json($category, 204);  
        } else{
            return response()->json($category,404);
        }
    }
}
